ChangeLog
=========

.. contents:: Releases
   :backlinks: none
   :local:

Unreleased
----------

1.0.0 (2024-09-23)
------------------

* Upgrade ``fred-frgal`` to ``3.15+`` (#13).
* Update project setup.

0.3.1 (2024-11-06)
------------------

* Upgrade to fred-frgal ~= 3.15 (#13)

0.3.0 (2024-05-22)
------------------

* Forbid legal entity attach to eID (#12)
* Update setup (new Python versions, ruff, ...)

0.2.0 (2021-12-08)
------------------

* Let ``IdentityAttached`` exception pass (#10).
* Return ``ContactIdentifier`` from ``get_contact_id_by_handle`` (#11).

0.1.0 (2021-11-05)
------------------

Initial version.
