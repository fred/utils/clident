"""Fred-clident - a client library for a Fred identity."""

from .client import (
    ContactIdentifier,
    IdentityCheckerClient,
    IdentityClient,
    IdentityData,
    IdentityManagerClient,
    PlaceAddress,
)

__version__ = "1.0.0"

__all__ = [
    "ContactIdentifier",
    "IdentityCheckerClient",
    "IdentityClient",
    "IdentityData",
    "IdentityManagerClient",
    "PlaceAddress",
]
