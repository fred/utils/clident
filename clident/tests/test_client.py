from datetime import date
from typing import Any, Dict, List, Sequence
from unittest import TestCase
from unittest.mock import call, sentinel
from uuid import UUID

from fred_api.identity.common_types_pb2 import ContactId as ContactIdMessage, Uuid
from fred_api.identity.service_identity_checker_grpc_pb2 import (
    CheckCurrentContactDataReply,
    CheckCurrentContactDataRequest,
)
from fred_api.identity.service_identity_manager_grpc_pb2 import (
    AttachIdentityReply,
    AttachIdentityRequest,
    DetachIdentityReply,
    DetachIdentityRequest,
    GetContactIdByHandleReply,
    GetContactIdByHandleRequest,
    UpdateIdentityRequest,
)
from fred_api.identity.service_identity_user_grpc_pb2 import GetContactsByIdentityReply, GetContactsByIdentityRequest
from fred_types import ContactHistoryId, ContactId
from frgal.utils import TestClientMixin
from google.protobuf.empty_pb2 import Empty
from grpc import RpcError, StatusCode
from grpc._channel import _RPCState, _SingleThreadedRendezvous as _Rendezvous

from clident.client import (
    ContactIdentifier,
    IdentityCheckerClient,
    IdentityClient,
    IdentityData,
    IdentityDecoder,
    IdentityManagerClient,
    PlaceAddress,
)
from clident.exceptions import IdentityAttached


class ContactIdentifierTest(TestCase):
    def test_id(self):
        ident = ContactIdentifier(UUID(int=42))
        self.assertEqual(ident.id, ContactId(UUID(int=42)))


class IdentityDecoderTest(TestCase):
    def test_decode_contact_id(self):
        decoder = IdentityDecoder()
        uuid = UUID(int=42)
        data = (
            # contact_id, ident
            (ContactIdMessage(uuid=Uuid(value=str(uuid))), ContactIdentifier(uuid=uuid)),
            (
                ContactIdMessage(uuid=Uuid(value=str(uuid)), handle="kryten"),
                ContactIdentifier(uuid=uuid, handle="kryten"),
            ),
            (
                ContactIdMessage(uuid=Uuid(value=str(uuid)), numeric_id=196),
                ContactIdentifier(uuid=uuid, numeric_id=196),
            ),
        )
        for contact_id, ident in data:
            with self.subTest(contact_id=contact_id):
                self.assertEqual(decoder.decode(contact_id), ident)

    def test_decode_uuid(self):
        decoder = IdentityDecoder()
        data = (
            # proto, uuid
            (Uuid(value=str(UUID(int=0))), UUID(int=0)),
            (Uuid(value=str(UUID(int=42))), UUID(int=42)),
            (Uuid(value=str(UUID(int=255))), UUID(int=255)),
            (Uuid(), None),
        )
        for proto, uuid in data:
            with self.subTest(proto=proto):
                self.assertEqual(decoder.decode(proto), uuid)


class TestIdentityClient(TestClientMixin, IdentityClient):
    """Testing version of an IdentityClient."""


class IdentityClientTest(TestCase):
    def setUp(self):
        self.client = TestIdentityClient(sentinel.netloc)

    def _test_get_contacts_by_identity(
        self, contact_ids: List[ContactIdMessage], result: Sequence[ContactIdentifier]
    ) -> None:
        subject = "Kryten"
        issuer = "DivaDroid"
        reply = GetContactsByIdentityReply()
        reply.data.contact_ids.extend(contact_ids)
        self.client.mock.return_value = reply

        ids = self.client.get_contacts_by_identity(subject, issuer)

        self.assertEqual(ids, result)
        request = GetContactsByIdentityRequest()
        request.identity.subject.value = subject
        request.identity.identity_provider.value = issuer
        self.assertEqual(
            self.client.mock.mock_calls,
            [call(request, method="/Fred.Identity.Api.IdentityUser/get_contacts_by_identity", timeout=None)],
        )

    def test_get_contacts_by_identity_empty(self):
        self._test_get_contacts_by_identity([], [])

    def test_get_contacts_by_identity(self):
        uuid = UUID(int=42)
        contact_id = ContactIdMessage()
        contact_id.uuid.value = str(uuid)
        self._test_get_contacts_by_identity([contact_id], [ContactIdentifier(uuid)])

    def test_get_contacts_by_identity_old_ids(self):
        uuid = UUID(int=42)
        contact_id = ContactIdMessage(handle="kryten", numeric_id=196)
        contact_id.uuid.value = str(uuid)
        self._test_get_contacts_by_identity([contact_id], [ContactIdentifier(uuid, handle="kryten", numeric_id=196)])

    def test_get_contacts_by_identity_multiple(self):
        uuid = UUID(int=42)
        contact_id = ContactIdMessage()
        contact_id.uuid.value = str(uuid)
        uuid_2 = UUID(int=42)
        contact_id_2 = ContactIdMessage()
        contact_id_2.uuid.value = str(uuid_2)
        self._test_get_contacts_by_identity(
            [contact_id, contact_id_2], [ContactIdentifier(uuid), ContactIdentifier(uuid_2)]
        )

    def test_get_contacts_by_identity_error(self):
        subject = "Kryten"
        issuer = "DivaDroid"
        error = _Rendezvous(_RPCState((), "", "", StatusCode.UNKNOWN, ""), None, None, None)
        self.client.mock.side_effect = error

        with self.assertRaisesRegex(RpcError, "StatusCode.UNKNOWN"):
            self.client.get_contacts_by_identity(subject, issuer)

        request = GetContactsByIdentityRequest()
        request.identity.subject.value = subject
        request.identity.identity_provider.value = issuer
        self.assertEqual(
            self.client.mock.mock_calls,
            [call(request, method="/Fred.Identity.Api.IdentityUser/get_contacts_by_identity", timeout=None)],
        )


class TestIdentityManagerClient(TestClientMixin, IdentityManagerClient):
    """Testing version of an IdentityManagerClient."""


class IdentityManagerClientTest(TestCase):
    def setUp(self):
        self.client = TestIdentityManagerClient(sentinel.netloc)

    def test_attach_identity(self):
        contact_id = ContactId("2X4B")
        subject = "Kryten"
        issuer = "DivaDroid"
        self.client.mock.return_value = AttachIdentityReply()

        self.client.attach_identity(contact_id, subject, issuer)

        request = AttachIdentityRequest()
        request.contact_id.uuid.value = contact_id
        request.identity.subject.value = subject
        request.identity.identity_provider.value = issuer
        self.assertEqual(
            self.client.mock.mock_calls,
            [call(request, method="/Fred.Identity.Api.IdentityManager/attach_identity", timeout=None)],
        )

    def test_attach_identity_uuid(self):
        uuid = UUID(int=42)
        subject = "Kryten"
        issuer = "DivaDroid"
        self.client.mock.return_value = AttachIdentityReply()

        with self.assertWarnsRegex(DeprecationWarning, "UUID type is deprecated for contact argument."):
            self.client.attach_identity(uuid, subject, issuer)

        request = AttachIdentityRequest()
        request.contact_id.uuid.value = str(uuid)
        request.identity.subject.value = subject
        request.identity.identity_provider.value = issuer
        self.assertEqual(
            self.client.mock.mock_calls,
            [call(request, method="/Fred.Identity.Api.IdentityManager/attach_identity", timeout=None)],
        )

    def test_attach_identity_already_attached(self):
        contact_id = ContactId("2X4B")
        subject = "Kryten"
        issuer = "DivaDroid"
        reply = AttachIdentityReply()
        reply.exception.identity_attached.CopyFrom(AttachIdentityReply.IdentityAttached())
        self.client.mock.return_value = reply

        with self.assertRaises(IdentityAttached):
            self.client.attach_identity(contact_id, subject, issuer)

        request = AttachIdentityRequest()
        request.contact_id.uuid.value = contact_id
        request.identity.subject.value = subject
        request.identity.identity_provider.value = issuer
        self.assertEqual(
            self.client.mock.mock_calls,
            [call(request, method="/Fred.Identity.Api.IdentityManager/attach_identity", timeout=None)],
        )

    def test_attach_identity_error(self):
        contact_id = ContactId("2X4B")
        subject = "Kryten"
        issuer = "DivaDroid"
        error = _Rendezvous(_RPCState((), "", "", StatusCode.UNKNOWN, ""), None, None, None)
        self.client.mock.side_effect = error

        with self.assertRaisesRegex(RpcError, "StatusCode.UNKNOWN"):
            self.client.attach_identity(contact_id, subject, issuer)

        request = AttachIdentityRequest()
        request.contact_id.uuid.value = contact_id
        request.identity.subject.value = subject
        request.identity.identity_provider.value = issuer
        self.assertEqual(
            self.client.mock.mock_calls,
            [call(request, method="/Fred.Identity.Api.IdentityManager/attach_identity", timeout=None)],
        )

    def test_detach_identity(self):
        contact_id = ContactId("2X4B")
        subject = "Kryten"
        issuer = "DivaDroid"
        self.client.mock.return_value = DetachIdentityReply()

        self.client.detach_identity(contact_id, subject, issuer)

        request = DetachIdentityRequest()
        request.contact_id.uuid.value = contact_id
        request.identity.subject.value = subject
        request.identity.identity_provider.value = issuer
        self.assertEqual(
            self.client.mock.mock_calls,
            [call(request, method="/Fred.Identity.Api.IdentityManager/detach_identity", timeout=None)],
        )

    def test_detach_identity_uuid(self):
        uuid = UUID(int=42)
        subject = "Kryten"
        issuer = "DivaDroid"
        self.client.mock.return_value = DetachIdentityReply()

        with self.assertWarnsRegex(DeprecationWarning, "UUID type is deprecated for contact argument."):
            self.client.detach_identity(uuid, subject, issuer)

        request = DetachIdentityRequest()
        request.contact_id.uuid.value = str(uuid)
        request.identity.subject.value = subject
        request.identity.identity_provider.value = issuer
        self.assertEqual(
            self.client.mock.mock_calls,
            [call(request, method="/Fred.Identity.Api.IdentityManager/detach_identity", timeout=None)],
        )

    def test_detach_identity_error(self):
        contact_id = ContactId("2X4B")
        subject = "Kryten"
        issuer = "DivaDroid"
        error = _Rendezvous(_RPCState((), "", "", StatusCode.UNKNOWN, ""), None, None, None)
        self.client.mock.side_effect = error

        with self.assertRaisesRegex(RpcError, "StatusCode.UNKNOWN"):
            self.client.detach_identity(contact_id, subject, issuer)

        request = DetachIdentityRequest()
        request.contact_id.uuid.value = contact_id
        request.identity.subject.value = subject
        request.identity.identity_provider.value = issuer
        self.assertEqual(
            self.client.mock.mock_calls,
            [call(request, method="/Fred.Identity.Api.IdentityManager/detach_identity", timeout=None)],
        )

    def test_update_identity(self):
        subject = "Kryten"
        issuer = "DivaDroid"
        address = PlaceAddress(
            street=["Deck 16"], city="Red Dwarf", state_or_province="Deep Space", postal_code="JMC", country_code="JU"
        )
        data = IdentityData("Kryten 2X4B-523P", date(1988, 9, 6), address)
        log_entry_id = 42
        self.client.mock.return_value = Empty()

        self.client.update_identity(subject, issuer, data, log_entry_id)

        request = UpdateIdentityRequest()
        request.identity.subject.value = subject
        request.identity.identity_provider.value = issuer
        request.identity_data.name = data.name
        request.identity_data.birthdate.year = data.birth_date.year
        request.identity_data.birthdate.month = data.birth_date.month
        request.identity_data.birthdate.day = data.birth_date.day
        request.identity_data.address.street.extend(address.street)
        request.identity_data.address.city = address.city
        request.identity_data.address.state_or_province = address.state_or_province
        request.identity_data.address.postal_code = address.postal_code
        request.identity_data.address.country_code = address.country_code
        request.log_entry_id.value = log_entry_id
        self.assertEqual(
            self.client.mock.mock_calls,
            [call(request, method="/Fred.Identity.Api.IdentityManager/update_identity", timeout=None)],
        )

    def test_update_identity_no_address(self):
        subject = "Kryten"
        issuer = "DivaDroid"
        data = IdentityData("Kryten 2X4B-523P", date(1988, 9, 6))
        log_entry_id = 42
        self.client.mock.return_value = Empty()

        self.client.update_identity(subject, issuer, data, log_entry_id)

        request = UpdateIdentityRequest()
        request.identity.subject.value = subject
        request.identity.identity_provider.value = issuer
        request.identity_data.name = data.name
        request.identity_data.birthdate.year = data.birth_date.year
        request.identity_data.birthdate.month = data.birth_date.month
        request.identity_data.birthdate.day = data.birth_date.day
        request.log_entry_id.value = log_entry_id
        self.assertEqual(
            self.client.mock.mock_calls,
            [call(request, method="/Fred.Identity.Api.IdentityManager/update_identity", timeout=None)],
        )

    def test_update_identity_error(self):
        subject = "Kryten"
        issuer = "DivaDroid"
        data = IdentityData("Kryten 2X4B-523P", date(1988, 9, 6))
        log_entry_id = 42
        error = _Rendezvous(_RPCState((), "", "", StatusCode.UNKNOWN, ""), None, None, None)
        self.client.mock.side_effect = error

        with self.assertRaisesRegex(RpcError, "StatusCode.UNKNOWN"):
            self.client.update_identity(subject, issuer, data, log_entry_id)

        request = UpdateIdentityRequest()
        request.identity.subject.value = subject
        request.identity.identity_provider.value = issuer
        request.identity_data.name = data.name
        request.identity_data.birthdate.year = data.birth_date.year
        request.identity_data.birthdate.month = data.birth_date.month
        request.identity_data.birthdate.day = data.birth_date.day
        request.log_entry_id.value = log_entry_id
        self.assertEqual(
            self.client.mock.mock_calls,
            [call(request, method="/Fred.Identity.Api.IdentityManager/update_identity", timeout=None)],
        )

    def test_get_contact_id_by_handle(self):
        handle = "kryten"
        uuid = UUID(int=42)
        numeric_id = 196
        reply = GetContactIdByHandleReply()
        reply.data.contact_id.uuid.value = str(uuid)
        reply.data.contact_id.handle = handle
        reply.data.contact_id.numeric_id = numeric_id
        self.client.mock.return_value = reply

        result = self.client.get_contact_id_by_handle(handle)

        self.assertEqual(result, ContactIdentifier(uuid, handle="kryten", numeric_id=196))
        request = GetContactIdByHandleRequest()
        request.contact_handle.value = handle
        self.assertEqual(
            self.client.mock.mock_calls,
            [call(request, method="/Fred.Identity.Api.IdentityManager/get_contact_id_by_handle", timeout=None)],
        )


class TestIdentityCheckerClient(TestClientMixin, IdentityCheckerClient):
    """Testing version of an IdentityCheckerClient."""


class IdentityCheckerClientTest(TestCase):
    def setUp(self):
        self.client = TestIdentityCheckerClient(sentinel.netloc)

    def test_check(self):
        contact_id = ContactId("2X4B")
        name = "Arnold Rimmer"
        auth_info = "Gazpacho!"
        history_id = UUID(int=128)
        reply = CheckCurrentContactDataReply()
        reply.data.contact_history_id.uuid.value = str(history_id)
        self.client.mock.return_value = reply

        response = self.client.check_current_contact_data(contact=contact_id, auth_info=auth_info, name=name)

        result = {"address": None, "auth_info_matches": False, "contact_history_id": history_id, "name": None}
        self.assertEqual(response, result)
        request = CheckCurrentContactDataRequest()
        request.contact_id.uuid.value = contact_id
        request.auth_info = auth_info
        request.name = name
        self.assertEqual(
            self.client.mock.mock_calls,
            [call(request, method="/Fred.Identity.Api.IdentityChecker/check_current_contact_data", timeout=None)],
        )

    def test_check_history_id(self):
        contact_id = ContactId("2X4B")
        name = "Arnold Rimmer"
        auth_info = "Gazpacho!"
        history_id = ContactHistoryId(UUID(int=128))
        reply = CheckCurrentContactDataReply()
        reply.data.contact_history_id.uuid.value = str(history_id)
        self.client.mock.return_value = reply

        response = self.client.check_current_contact_data(
            contact=contact_id, history_id=history_id, auth_info=auth_info, name=name
        )

        result = {"address": None, "auth_info_matches": False, "contact_history_id": UUID(history_id), "name": None}
        self.assertEqual(response, result)
        request = CheckCurrentContactDataRequest()
        request.contact_id.uuid.value = contact_id
        request.contact_history_id.uuid.value = history_id
        request.auth_info = auth_info
        request.name = name
        self.assertEqual(
            self.client.mock.mock_calls,
            [call(request, method="/Fred.Identity.Api.IdentityChecker/check_current_contact_data", timeout=None)],
        )

    def test_check_uuid(self):
        uuid = UUID(int=42)
        name = "Arnold Rimmer"
        auth_info = "Gazpacho!"
        history_id = UUID(int=128)
        reply = CheckCurrentContactDataReply()
        reply.data.contact_history_id.uuid.value = str(history_id)
        self.client.mock.return_value = reply

        with self.assertWarnsRegex(DeprecationWarning, "UUID type is deprecated for contact argument."):
            response = self.client.check_current_contact_data(contact=uuid, auth_info=auth_info, name=name)

        result = {"address": None, "auth_info_matches": False, "contact_history_id": history_id, "name": None}
        self.assertEqual(response, result)
        request = CheckCurrentContactDataRequest()
        request.contact_id.uuid.value = str(uuid)
        request.auth_info = auth_info
        request.name = name
        self.assertEqual(
            self.client.mock.mock_calls,
            [call(request, method="/Fred.Identity.Api.IdentityChecker/check_current_contact_data", timeout=None)],
        )

    def test_check_history_uuid(self):
        contact_id = ContactId("2X4B")
        name = "Arnold Rimmer"
        auth_info = "Gazpacho!"
        history_id = UUID(int=128)
        reply = CheckCurrentContactDataReply()
        reply.data.contact_history_id.uuid.value = str(history_id)
        self.client.mock.return_value = reply

        with self.assertWarnsRegex(DeprecationWarning, "UUID type is deprecated for history_id argument."):
            response = self.client.check_current_contact_data(
                contact=contact_id, history_id=history_id, auth_info=auth_info, name=name
            )

        result = {"address": None, "auth_info_matches": False, "contact_history_id": history_id, "name": None}
        self.assertEqual(response, result)
        request = CheckCurrentContactDataRequest()
        request.contact_id.uuid.value = contact_id
        request.contact_history_id.uuid.value = str(history_id)
        request.auth_info = auth_info
        request.name = name
        self.assertEqual(
            self.client.mock.mock_calls,
            [call(request, method="/Fred.Identity.Api.IdentityChecker/check_current_contact_data", timeout=None)],
        )

    def test_check_all_data(self):
        contact_id = ContactId("2X4B")
        name = "Arnold Rimmer"
        auth_info = "Gazpacho!"
        address = PlaceAddress(
            street=["Deck 16"], city="Red Dwarf", state_or_province="Deep Space", postal_code="JMC", country_code="JU"
        )
        birth_date = date(1988, 9, 6)
        history_id = ContactHistoryId(UUID(int=128))
        reply = CheckCurrentContactDataReply()
        reply.data.contact_history_id.uuid.value = str(history_id)
        reply.data.address.city = "Red Dwarf"
        reply.data.address.postal_code = "11111"
        self.client.mock.return_value = reply

        response = self.client.check_current_contact_data(
            contact=contact_id,
            auth_info=auth_info,
            name=name,
            history_id=history_id,
            address=address,
            birth_date=birth_date,
        )

        address_result: Dict[str, Any] = {
            "street": [],
            "city": "Red Dwarf",
            "state_or_province": "",
            "postal_code": "11111",
            "country_code": "",
        }
        result = {
            "address": address_result,
            "auth_info_matches": False,
            "contact_history_id": UUID(history_id),
            "name": None,
        }
        self.assertEqual(response, result)
        request = CheckCurrentContactDataRequest()
        request.contact_id.uuid.value = contact_id
        request.contact_history_id.uuid.value = str(history_id)
        request.auth_info = auth_info
        request.name = name
        request.address.street.extend(address.street)
        request.address.city = address.city
        request.address.state_or_province = address.state_or_province
        request.address.postal_code = address.postal_code
        request.address.country_code = address.country_code
        request.birthdate.year = birth_date.year
        request.birthdate.month = birth_date.month
        request.birthdate.day = birth_date.day
        self.assertEqual(
            self.client.mock.mock_calls,
            [call(request, method="/Fred.Identity.Api.IdentityChecker/check_current_contact_data", timeout=None)],
        )
