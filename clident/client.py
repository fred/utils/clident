"""Identity gRPC client."""

import warnings
from dataclasses import dataclass
from datetime import date
from typing import Any, Dict, Optional, Sequence, Union, cast
from uuid import UUID

from fred_api.identity.common_types_pb2 import (
    ContactDoesNotExist as ContactDoesNotExistMessage,
    ContactId as ContactIdMessage,
    ContactIsOrganization as ContactIsOrganizationMessage,
    HistoryIdMismatch as HistoryIdMismatchMessage,
    IdentityDoesNotExist as IdentityDoesNotExistMessage,
    Uuid,
)
from fred_api.identity.service_identity_checker_grpc_pb2 import CheckCurrentContactDataRequest
from fred_api.identity.service_identity_checker_grpc_pb2_grpc import IdentityCheckerStub
from fred_api.identity.service_identity_manager_grpc_pb2 import (
    AttachIdentityReply,
    AttachIdentityRequest,
    DetachIdentityRequest,
    GetContactIdByHandleRequest,
    UpdateIdentityRequest,
)
from fred_api.identity.service_identity_manager_grpc_pb2_grpc import IdentityManagerStub
from fred_api.identity.service_identity_user_grpc_pb2 import GetContactsByIdentityRequest
from fred_api.identity.service_identity_user_grpc_pb2_grpc import IdentityUserStub
from fred_types import ContactHistoryId, ContactId
from frgal import GrpcClient, GrpcDecoder

from .exceptions import (
    ContactDoesNotExist,
    ContactIsOrganization,
    HistoryIdMismatch,
    IdentityAttached,
    IdentityDoesNotExist,
)


@dataclass
class ContactIdentifier:
    """Contains contact UUID along with identifiers for old interfaces."""

    uuid: UUID
    handle: str = ""
    numeric_id: int = 0

    @property
    def id(self) -> ContactId:
        """Return contact identifier."""
        return ContactId(self.uuid)


@dataclass
class PlaceAddress:
    """Represents PlaceAddress from interface."""

    street: Sequence[str] = ()
    city: str = ""
    state_or_province: str = ""
    postal_code: str = ""
    country_code: str = ""


@dataclass
class IdentityData:
    """Represents IdentityData from interface."""

    name: str
    birth_date: date
    address: Optional[PlaceAddress] = None


class IdentityDecoder(GrpcDecoder):
    """Decoder for identity client."""

    decode_unset_messages = False
    decode_empty_string_none = False

    def __init__(self) -> None:
        super().__init__()
        self.set_decoder(ContactIdMessage, self._decode_contact_id)
        self.set_decoder(Uuid, self._decode_uuid)
        self.set_exception_decoder(ContactDoesNotExistMessage, ContactDoesNotExist)
        self.set_exception_decoder(HistoryIdMismatchMessage, HistoryIdMismatch)
        self.set_exception_decoder(AttachIdentityReply.IdentityAttached, IdentityAttached)
        self.set_exception_decoder(IdentityDoesNotExistMessage, IdentityDoesNotExist)
        self.set_exception_decoder(ContactIsOrganizationMessage, ContactIsOrganization)

    def _decode_contact_id(self, value: ContactIdMessage) -> ContactIdentifier:
        return ContactIdentifier(UUID(value.uuid.value), handle=value.handle, numeric_id=value.numeric_id)

    def _decode_uuid(self, value: Uuid) -> Optional[UUID]:
        """Decode Uuid."""
        if not value.value:
            return None
        return UUID(value.value)


class IdentityClient(GrpcClient):
    """Identity gRPC client."""

    stub_cls = IdentityUserStub
    decoder_cls = IdentityDecoder

    def get_contacts_by_identity(self, subject: str, issuer: str) -> Sequence[ContactIdentifier]:
        """Return UUIDs of contact matching a provided identity."""
        request = GetContactsByIdentityRequest()
        request.identity.subject.value = subject
        request.identity.identity_provider.value = issuer
        response_data = self.call("get_contacts_by_identity", request)
        return cast(Sequence[ContactIdentifier], response_data)


class IdentityManagerClient(GrpcClient):
    """Identity manager gRPC client."""

    stub_cls = IdentityManagerStub
    decoder_cls = IdentityDecoder

    def attach_identity(self, contact: Union[ContactId, UUID], subject: str, issuer: str) -> None:
        """Attach identity to a contact.

        Raises:
            ContactDoesNotExist: The contact does not exist.
            HistoryIdMismatch: History ID is not a latest history ID of a contact.
            IdentityAttached: The identity was not attached due to existing relations on the contact.
            RpcError: An error in gRPC occurred.
        """
        if isinstance(contact, UUID):
            warnings.warn("UUID type is deprecated for contact argument.", DeprecationWarning, stacklevel=2)
        request = AttachIdentityRequest()
        request.contact_id.uuid.value = str(contact)
        request.identity.subject.value = subject
        request.identity.identity_provider.value = issuer
        return cast(None, self.call("attach_identity", request))

    def detach_identity(self, contact: Union[ContactId, UUID], subject: str, issuer: str) -> None:
        """Detach identity from a contact.

        Raises:
            ContactDoesNotExist: The contact does not exist.
            IdentityDoesNotExist: The identity is not attached to the contact.
            RpcError: An error in gRPC occurred.
        """
        if isinstance(contact, UUID):
            warnings.warn("UUID type is deprecated for contact argument.", DeprecationWarning, stacklevel=2)
        request = DetachIdentityRequest()
        request.contact_id.uuid.value = str(contact)
        request.identity.subject.value = subject
        request.identity.identity_provider.value = issuer
        return cast(None, self.call("detach_identity", request))

    def update_identity(self, subject: str, issuer: str, data: IdentityData, log_entry_id: int) -> None:
        """Update identity data.

        Raises:
            RpcError: An error in gRPC occurred.
        """
        request = UpdateIdentityRequest()
        request.identity.subject.value = subject
        request.identity.identity_provider.value = issuer
        request.identity_data.name = data.name
        request.identity_data.birthdate.year = data.birth_date.year
        request.identity_data.birthdate.month = data.birth_date.month
        request.identity_data.birthdate.day = data.birth_date.day
        if data.address:
            request.identity_data.address.street.extend(data.address.street)
            request.identity_data.address.city = data.address.city
            request.identity_data.address.state_or_province = data.address.state_or_province
            request.identity_data.address.postal_code = data.address.postal_code
            request.identity_data.address.country_code = data.address.country_code
        request.log_entry_id.value = log_entry_id
        return cast(None, self.call("update_identity", request))

    def get_contact_id_by_handle(self, handle: str) -> ContactIdentifier:
        """Return UUID for contact handle.

        Raises:
            ContactDoesNotExist: The contact does not exist.
            RpcError: An error in gRPC occurred.
        """
        request = GetContactIdByHandleRequest()
        request.contact_handle.value = handle
        reply = self.call("get_contact_id_by_handle", request)
        return cast(ContactIdentifier, reply)


class IdentityCheckerClient(GrpcClient):
    """Identity checker gRPC client."""

    stub_cls = IdentityCheckerStub
    decoder_cls = IdentityDecoder

    def check_current_contact_data(
        self,
        *,
        contact: Union[ContactId, UUID],
        history_id: Optional[Union[ContactHistoryId, UUID]] = None,
        auth_info: str,
        name: str,
        address: Optional[PlaceAddress] = None,
        birth_date: Optional[date] = None,
    ) -> Dict[str, Any]:
        """Check current contact data.

        Raises:
            ContactDoesNotExist: The contact does not exist.
            HistoryIdMismatch: History ID is not a latest history ID of the contact.
            RpcError: An error in gRPC occurred.
        """
        if isinstance(contact, UUID):
            warnings.warn("UUID type is deprecated for contact argument.", DeprecationWarning, stacklevel=2)
        if isinstance(history_id, UUID):
            warnings.warn("UUID type is deprecated for history_id argument.", DeprecationWarning, stacklevel=2)
        request = CheckCurrentContactDataRequest()
        request.contact_id.uuid.value = str(contact)
        if history_id is not None:
            request.contact_history_id.uuid.value = str(history_id)
        request.auth_info = auth_info
        request.name = name
        if address:
            request.address.street.extend(address.street)
            request.address.city = address.city
            request.address.state_or_province = address.state_or_province
            request.address.postal_code = address.postal_code
            request.address.country_code = address.country_code
        if birth_date:
            request.birthdate.year = birth_date.year
            request.birthdate.month = birth_date.month
            request.birthdate.day = birth_date.day
        return cast(Dict[str, Any], self.call("check_current_contact_data", request))
