"""Clident specific exceptions."""


class ClidentError(Exception):
    """Base class for all clident errors."""


class ContactDoesNotExist(ClidentError, ValueError):
    """Contact does not exist."""


class ContactIsOrganization(ClidentError, ValueError):
    """Contact is organization (legal entity)."""


class HistoryIdMismatch(ClidentError, ValueError):
    """History ID mismatch."""


class IdentityAttached(ClidentError, ValueError):
    """The identity is already attached to the contact."""


class IdentityDoesNotExist(ClidentError, ValueError):
    """The identity is not attached to the contact."""
